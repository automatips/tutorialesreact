import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Table, Button, Container, Modal, ModalBody, ModalHeader, FormGroup, ModalFooter } from 'reactstrap'

const data = [
  { id: 1, character: "Naruto", anime: "Naruto" },
  { id: 2, character: "Vegueta", anime: "Dragon Ball Z" },
  { id: 3, character: "Kenshin Himura", anime: "Rurouni Kenshin" },
  { id: 4, character: "Monkey D. Luffy", anime: "One Piece" },
  { id: 5, character: "Edward Elric", anime: "Full Metal Alchemist: Brotherhood" },
  { id: 6, character: "Seto Kaiba", anime: "Yu-Gi-Oh!" },
  { id: 7, character: "Saitama", anime: "One Punch Man" },

];

class App extends React.Component {
  state = {
    data: data,
    form: {
      id: '',
      character: '',
      anime: ''
    },
    modalInsert: false,
    modalEdit: false,
  };

  handleChange = e => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      }
    });
  }

  mostrarModalInsert = () => {
    this.setState({ modalInsert: true});
  }

  ocultarModalInsert = () => {
    this.setState({ modalInsert: false});
  }

  mostrarModalEdit = (record) => {
    this.setState({ modalEdit: true, form: record});
  }

  ocultarModalEdit = () => {
    this.setState({ modalEdit: false});
  }


  insert = () => {
    var valorNuevo = { ...this.state.form };
    valorNuevo.id = this.state.data.length + 1;
    var list = this.state.data;
    list.push(valorNuevo);
    this.setState({ data: list, modalInsert: false });
  }

  edit=(dato)=>{
    var contador=0;
    var list=this.state.data;
    list.map((record)=>{
      if(dato.id==record.id){
        list[contador].character=dato.character;
        list[contador].anime=dato.anime;
      }
      contador++;
    });
    this.setState({data: list, modalEdit: false});
  }

  delete=(dato)=>{
    var opcion=window.confirm("do you really want to delete the record "+dato.id);
      if(opcion){
        var contador=0;
        var list = this.state.data;
        list.map((record)=>{
          if(record.id==dato.id){
            list.splice(contador, 1);
          }
          contador++;
        });
        this.setState({data: list});
      }
    }    

  render() {
    return (
      <>
        <Container>
          <br />
          <Button color="success" onClick={() => this.mostrarModalInsert()}>Insert new character</Button>
          <br />
          <br />

          <Table>
            <thead>
              <tr>
                <th>Id</th>
                <th>Character</th>
                <th>Anime</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              {this.state.data.map((elemento) => (
                <tr>
                  <td>{elemento.id}</td>
                  <td>{elemento.character}</td>
                  <td>{elemento.anime}</td>
                  <td><Button color="primary" onClick={() => this.mostrarModalEdit(elemento)}>Edit</Button>{"  "}
                    <Button color="danger" onClick={() => this.delete(elemento)}>Delete</Button></td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Container>

        <Modal isOpen={this.state.modalInsert}>
          <ModalHeader>
            <div>
              <h3>Insert Record</h3>
            </div>
          </ModalHeader>

          <ModalBody>
            <FormGroup>
              <label>Id:</label>
              <input className="form-control" readOnly type="text" value={this.state.data.length + 1} />
            </FormGroup>

            <FormGroup>
              <label>characters:</label>
              <input className="form-control" name="character" type="text" onChange={this.handleChange} />
            </FormGroup>

            <FormGroup>
              <label>Anime:</label>
              <input className="form-control" name="anime" type="text" onChange={this.handleChange} />
            </FormGroup>
          </ModalBody>

          <ModalFooter>
            <Button color="primary" onClick={() => this.insert()} >Insert</Button>
            <Button color="danger" onClick={() => this.ocultarModalInsert()}>Cancel</Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.modalEdit}>
          <ModalHeader>
            <div>
              <h3>Edit Record</h3>
            </div>
          </ModalHeader>

          <ModalBody>
            <FormGroup>
              <label>Id:</label>
              <input className="form-control" readOnly type="text" value={this.state.form.id}/>
            </FormGroup>

            <FormGroup>
              <label>Character:</label>
              <input className="form-control" name="character" type="text" onChange={this.handleChange} value={this.state.form.character}/>
            </FormGroup>

            <FormGroup>
              <label>Anime:</label>
              <input className="form-control" name="anime" type="text" onChange={this.handleChange} value={this.state.form.anime}/>
            </FormGroup>
          </ModalBody>

          <ModalFooter>
            <Button color="primary" onClick={() => this.edit(this.state.form)}>Edit</Button>
            <Button color="danger"  onClick={() => this.ocultarModalEdit()}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </>
    );
  }
}

export default App;